﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using System.Data.SQLite;
using Dapper;
using System.Windows.Data;
using System.Globalization;

namespace ToDoList
{
    public enum States { InQueue, InProgress, Done };
    public class ApplicationViewModel : INotifyPropertyChanged
    {
        string connectionString = @"Data Source=.\TasksBase.db";

        public ObservableCollection<Task> tasksDone;
        public ObservableCollection<Task> tasksInQueue;
        public ObservableCollection<Task> tasksInProgress;

        public ObservableCollection<Task> TasksDone
        {
            get { return tasksDone; }
            set
            {
                tasksDone = value;
                OnPropertyChanged("TasksDone");
            }
        }
        public ObservableCollection<Task> TasksInQueue
        {
            get { return tasksInQueue; }
            set
            {
                tasksInQueue = value;
                OnPropertyChanged("TasksInQueue");
            }
        }
        public ObservableCollection<Task> TasksInProgress
        {
            get { return tasksInProgress; }
            set
            {
                tasksInProgress = value;
                OnPropertyChanged("TasksInProgress");
            }
        }

        private Task selectedTask;
        public Task SelectedTask
        {
            get
            { 
                return selectedTask;
            }
            set
            {
                selectedTask = value;
                OnPropertyChanged("SelectedTask");
            }
        }

        public ApplicationViewModel()
        {
            TasksDone = new ObservableCollection<Task>();
            TasksInQueue = new ObservableCollection<Task>();
            TasksInProgress = new ObservableCollection<Task>();

            var connection = new SQLiteConnection(connectionString);
            connection.Open();

            connection.Query<Task>("CREATE TABLE IF NOT EXISTS \"Tasks\" (\"Id\" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, \"Name\"  TEXT, \"Discription\"   TEXT, \"State\" INTEGER NOT NULL)");

            var TasksTemp = connection.Query<Task>("SELECT * FROM Tasks");
            foreach (var localtask in TasksTemp)
            {
                if(localtask.State == States.InQueue) TasksInQueue.Add(localtask);
                else
                {
                    if (localtask.State == States.InProgress) TasksInProgress.Add(localtask);
                    else TasksDone.Add(localtask);
                }        
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        
        private RelayCommand addCommand;
        public RelayCommand AddCommand
        {
            get
            {
                return addCommand ??
                  (addCommand = new RelayCommand(obj =>
                  {
                      Task newTask = new Task();
                      newTask.Name = "Новое задание";
                      newTask.Discription = "Описание";

                      var connection = new SQLiteConnection(connectionString);
                      connection.Open();

                      connection.Query<Task>("INSERT INTO Tasks (Name, Discription, State) VALUES('Новое задание', 'Описание', 0)");
                      var id = connection.Query<long>("SELECT last_insert_rowid();");

                      foreach (var result in id)
                          newTask.Id = result;

                      TasksInQueue.Insert(0, newTask);
                      SelectedTask = newTask;
                  }));
            }
        }
        
        private RelayCommand removeCommand;
        public RelayCommand RemoveCommand
        {
            get
            {
                return removeCommand ??
                    (removeCommand = new RelayCommand(obj =>
                    {
                        Task newTask = obj as Task;
                        if (newTask != null)
                        {
                            var connection = new SQLiteConnection(connectionString);
                            connection.Open();

                            connection.Query<Task>("DELETE FROM Tasks WHERE id = @taskId", new { taskId = newTask.Id });
                            if (newTask.State == States.InQueue) TasksInQueue.Remove(newTask);
                            else
                            {
                                if (newTask.State == States.InProgress) TasksInProgress.Remove(newTask);
                                else TasksDone.Remove(newTask);
                            }
                        }
                    },
                    (obj) => SelectedTask != null));
            }
        }

        private RelayCommand setState;
        public RelayCommand SetState
        {
            get
            {
                return setState ??
                    (setState = new RelayCommand(obj =>
                    {
                        var st = obj;

                        Task newTask = SelectedTask;
                        if (newTask != null)
                        {
                            var connection = new SQLiteConnection(connectionString);
                            connection.Open();

                            connection.Query<Task>("UPDATE Tasks SET State = @taskState WHERE id = @taskId", new { taskState = st, taskId = newTask.Id });

                            switch (newTask.State)
                            {
                                case States.InQueue:
                                    TasksInQueue.Remove(newTask);
                                    break;
                                case States.InProgress:
                                    TasksInProgress.Remove(newTask);
                                    break;
                                case States.Done:
                                    TasksDone.Remove(newTask);
                                    break;
                            }

                            switch (st)
                            {
                                case "0":
                                    TasksInQueue.Insert(0, newTask);
                                    newTask.State = States.InQueue;
                                    break;
                                case "1":
                                    TasksInProgress.Insert(0, newTask);
                                    newTask.State = States.InProgress;
                                    break;
                                case "2":
                                    TasksDone.Insert(0, newTask);
                                    newTask.State = States.Done;
                                    break;
                            }
                        }
                    },
                    (obj) => SelectedTask != null));
            }
        }

        private RelayCommand saveCommand;
        public RelayCommand SaveCommand
        {
            get
            {
                return saveCommand ??
                    (saveCommand = new RelayCommand(obj =>
                    {
                        Task newTask = obj as Task;
                        if (newTask != null)
                        { 
                            var connection = new SQLiteConnection(connectionString);
                            connection.Open();

                            connection.Query<Task>("UPDATE Tasks SET Name = @taskName, Discription = @taskDiscription, State = @taskState WHERE id = @taskId", 
                                                    new { taskName = newTask.Name, taskDiscription = newTask.Discription, taskState = newTask.State, taskId = newTask.Id });
                        }
                    },
                    (obj) => (TasksDone.Any() || TasksInQueue.Any() || TasksInProgress.Any()) && SelectedTask != null));
            }
        }
    }

    //////ICommand
    public class RelayCommand : ICommand
    {
        private Action<object> execute;
        private Func<object, bool> canExecute;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public RelayCommand(Action<object> execute, Func<object, bool> canExecute = null)
        {
            this.execute = execute;
            this.canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return this.canExecute == null || this.canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            this.execute(parameter);
        }
    }
}
